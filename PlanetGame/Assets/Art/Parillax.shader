﻿Shader "Custom/Parillax"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_OverlayTex("Texture", 2D) = "white" {}
		_Speed("Speed Multiplier", float) = 0.5
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			sampler2D _OverlayTex;
			float _Speed;
			//uniform float4 _OverlayTex_TexelSize;
			//int passes = 1;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);
				// apply fog
				////UNITY_APPLY_FOG(i.fogCoord, col);
				//float xPos = i.uv.x;

				//if (i.uv.x > _OverlayTex_TexelSize.x)
				//{

				//	xPos = i.uv.x;
				//}

				//if (i.uv.x > 100)
				//{
				//	xPos = i.uv.x - 100;
				//}

				//passes = floor(i.uv.x / _OverlayTex_TexelSize.x) + 1;

				//xPos = i.uv.x - (passes * _OverlayTex_TexelSize.x);
				

				col += tex2D(_OverlayTex, float2(i.uv.x, i.uv.y + _Time[1] * _Speed * 0.2));
				col /= 2;
				col += tex2D(_OverlayTex, float2(-i.uv.x, i.uv.y + _Time[1] * _Speed * 0.3));
				col /= 2;
				col += tex2D(_OverlayTex, float2(i.uv.x + 0.1, i.uv.y + _Time[1] * _Speed * 0.4));
				col /= 2;
				col += tex2D(_OverlayTex, float2(-i.uv.x + 0.1, i.uv.y + _Time[1] * _Speed * 0.5));
				col /= 2;

				//col += tex2D(_OverlayTex, float2(xPos + _Time[1] * _Speed * 0.25, i.uv.y));
				//col /= 2;
				//col += tex2D(_OverlayTex, float2(xPos + _Time[1] * _Speed * 0.5, i.uv.y));
				//col /= 2;


				return col;
			}
			ENDCG
		}
	}
}
