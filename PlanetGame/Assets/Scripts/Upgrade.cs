﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Upgrade", menuName = "Upgrade")]
public class Upgrade : ScriptableObject
{
    public enum UpgradeType { DPS }
    [Tooltip("Type of upgrade")]
    public UpgradeType upgradeType;
    [Tooltip("Cost to buy")]
    public int cost;
    [Tooltip("Value associated with upgrade. E.g if DPS, value of 10 would be +10 DPS")]
    public int value;

    public string upgradeName, description;
    
    public Sprite sprite;


    public void Activate()
    {
        switch (upgradeType)
        {
            case UpgradeType.DPS:
            PlayerController.AddDamage = value;
            break;
        }
    }
}
