﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    static int money, damagePerClick, planetsDestroyed;
    public static int DamagePerClick
    { get { return damagePerClick; } }

    public static int Money
    { 
        get { return money; } 
        set { if (value > -1) money = value; else Debug.LogError("Cannot set money to below 0"); }
    }
    public static int AddMoney
    {
        set { money += value; if (money < 0) money = 0; }
    }

    public static int AddDamage
    {
        set { damagePerClick += value; if (damagePerClick < 0) damagePerClick = 0; }
    }

    public static int PlanetsDestroyed
    {
        get { return planetsDestroyed; }
        set
        {
            planetsDestroyed = value;
            if (planetsDestroyed < 0)
                planetsDestroyed = 0;
        }
    }

    void Start()
    {
        money = 0;
        planetsDestroyed = 0;
        damagePerClick = 1;
    }
}
