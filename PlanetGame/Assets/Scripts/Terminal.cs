﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Terminal : MonoBehaviour
{
    bool isOpen = false;
    public InputField inputField;

    public Text commandDisplayText;
    GameObject terminalObject;

    public delegate void Delegate();

    static List<Command> allCommands = new List<Command>();

    public class Command
    {
        public string name;
        private Delegate function;
        protected Command()
        { allCommands.Add(this); }
        public Command(string name, Delegate function) : this()
        {
            this.name = name;
            this.function = function;
        }
        public void Call()
        {
            function();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        terminalObject = transform.GetChild(0).gameObject;
        inputField.onEndEdit.AddListener(delegate { RunCommand(); });
        Close();

        Command fadeIn = new Command("fadein", delegate { GameController.gameController.TriggerFadeIn(); } );
        Command newGalaxy = new Command("newgalaxy", delegate { GalaxyManager.CreateNewGalaxy(); } );
        Command nextSystem = new Command("nextsystem", delegate { SolarSystemManager.CycleSystems(1);} );
        Command rerender = new Command("rerender", delegate { PlanetManager.GetCurrentPlanetFocus.Render(); } );
        Command flash = new Command("flash", delegate { GameController.gameController.TriggerFlash(); } );
        Command money = new Command("bigmoney", delegate { PlayerController.AddMoney = 50000; } );
        //Command close = new Command("close", delegate { GameController.cameraMode = GameController.CameraMode.close; } );
        //Command system = new Command("system", delegate { GameController.cameraMode = GameController.CameraMode.system; } );
        //Command galaxy = new Command("galaxy", delegate { GameController.cameraMode = GameController.CameraMode.galaxy; } );
        Command kill = new Command("kill", delegate { PlanetManager.GetCurrentPlanetFocus.Damage(999999999, true); } );
        Command mute = new Command("mute", delegate { AudioManager.ToggleMute(); } );
        Command quit = new Command("quit", delegate { Application.Quit(); } );
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.BackQuote))
            Toggle();
    }

    void Open()
    {
        isOpen = true;
        terminalObject.SetActive(true);
        EventSystem.current.SetSelectedGameObject(terminalObject, null);
        commandDisplayText.text = GetCommandNames();
    }

    void Close()
    {
        isOpen = false;
        terminalObject.SetActive(false);
        EventSystem.current.SetSelectedGameObject(null);
        inputField.text = "";
    }

    void Toggle()
    {
        if (isOpen)
            Close();
        else
            Open();
    }

    void RunCommand()
    {
        if (inputField == null)
            return;

        try
        {
            FindCommand(inputField.text).Call();
        } 
        catch
        {
            Debug.Log("Terminal: Command not recognised.");
        }

        Close();
    }

    Command FindCommand(string name)
    {
        if (allCommands.Count == 0)
            Debug.LogError("Terminal: No commands");

        for (int i = 0; i < allCommands.Count; i++)
        {
            if (allCommands[i].name == name)
                return allCommands[i];
        }

        return null;
    }

    string GetCommandNames()
    {
        string returnValue = "";

        for (int i = 0; i < allCommands.Count; i++)
            returnValue += allCommands[i].name + "\n";

        return returnValue;
    }
}
