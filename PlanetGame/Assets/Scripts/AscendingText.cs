﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AscendingText : MonoBehaviour
{
    // Start is called before the first frame update

    Text text;

    const float SPEED = 10f, MAX_DISPLACEMENT = 100f, SPAWN_POSITION_VARIATION = 40f;

    Vector3 initialPosition;

    public string textToDisplay;

    void Start()
    {
        text = GetComponent<Text>();
        transform.Translate(new Vector3(
                Random.Range(-SPAWN_POSITION_VARIATION, SPAWN_POSITION_VARIATION),
                Random.Range(-SPAWN_POSITION_VARIATION, SPAWN_POSITION_VARIATION),
                0
                ));
        initialPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        text.text = textToDisplay;
        transform.Translate(Vector3.up * Time.deltaTime * SPEED);

        if (transform.position.x >= (initialPosition.x + MAX_DISPLACEMENT))
            Destroy(this.gameObject);

        text.color = Planet.SetColourAlpha(text.color, text.color.a - (Time.deltaTime * 2));
    }

    
}
