﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SatelliteBehaviour : MonoBehaviour
{
    // Start is called before the first frame update

    public Planet attachedPlanet;
    public SpriteRenderer sprite;
    int power;

    public float rotateSpeed;
    //public enum Effect { damage }
    //public Effect effect;
    public GameObject rotationBase;
    Satellite.SatelliteType satelliteType;
    void Start()
    {
        
    }

    // public void Construct(Planet _planet, Effect _effect, int _power)
    // {
    //     attachedPlanet = _planet;
    //     effect = _effect;
    //     power = _power;
    // }

    public void Set(Satellite satelliteInfo)
    {
        power = satelliteInfo.value;
        satelliteType = satelliteInfo.satelliteType;
        sprite.sprite = satelliteInfo.sprite;
    }

    void Update()
    {
        rotationBase.transform.Rotate(0, 0, Time.deltaTime * rotateSpeed);
    }

    public void RandomizeRotation()
    {
        rotationBase.transform.Rotate(0, 0, Random.Range(0, 360));
    }

    public void UpdateSatellite()
    {
        switch (satelliteType)
        {
            case Satellite.SatelliteType.DPS:
                if (!attachedPlanet.PopulationDepleted)
                    attachedPlanet.Damage(power, true);
            break;
            default:
            break;
        }
    }

}
