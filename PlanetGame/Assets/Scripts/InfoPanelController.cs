﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InfoPanelController : MonoBehaviour
{
    public static InfoPanelController infoPanelController;

    public bool isOpen = false;

    public static bool isMouseOver = false;
    public float movementSpeed = 10f;

    RectTransform panelRect;
    public RectTransform buttonRect;
    Button button;

    public Text nameIndicator, displayName, population;

    const float UPGRADE_ITEM_PANEL_Y_OFFSET = 10f;

    float Width
    { 
        get
        {
            return panelRect.sizeDelta.x;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        infoPanelController = this;
        isOpen = false;
        //buttonRect.gameObject.GetComponent<Button>();
        panelRect = GetComponent<RectTransform>();
        button =  buttonRect.gameObject.GetComponent<Button>();
        button.onClick.AddListener( delegate { Toggle(); } );
    }

    // Update is called once per frame
    void Update()
    {
        GameController.infoMenuIsOpen = isOpen;
        if (!isOpen)
        {
            LerpPanelToPosition(new Vector3(panelRect.sizeDelta.x, 0));
            LerpButtonToPosition(new Vector3(-buttonRect.sizeDelta.x, 0));
        }
        else
        {
            LerpPanelToPosition(new Vector3(0, 0));
            LerpButtonToPosition(new Vector3(0, 0));
        }
        isMouseOver = EventSystem.current.IsPointerOverGameObject();
        UpdateValues();
    }

    void Toggle()
    {
        if (isOpen)
            GameController.CloseMenus();
        else
        {
            GameController.CloseMenus();
            isOpen = true;
        }
    }

    void LerpPanelToPosition(Vector3 targetPosition)
    {
        panelRect.anchoredPosition = Vector3.Lerp(panelRect.anchoredPosition, targetPosition, Time.deltaTime * movementSpeed);
    }

    void LerpButtonToPosition(Vector3 targetPosition)
    {
        buttonRect.anchoredPosition = Vector3.Lerp(buttonRect.anchoredPosition, targetPosition, Time.deltaTime * movementSpeed * 0.7f);
    }

    void UpdateValues()
    {
        displayName.text = GameController.GetCurrectFocusObjectName();
        population.text = GameController.FormatNumber(GameController.GetCurrectFocusObjectPopulation());
        //population.text = GameController.GetCurrectFocusObjectPopulation().ToString();
        nameIndicator.text = GameController.ZoomLevelName;
    }
}
