﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//using UnityEngine.EventSystems;

public class PlanetManager : MonoBehaviour
{
    public static List<Planet> planets = new List<Planet>();

    static List<Planet> systemPlanets;

    float timer, averageValue;

    public const float TICK_TIME_MS = 0.1f,
                       FAR_VIEW_MULTIPLIER = 1.25f;

    //public Button left, right;

    public GameObject satellitePrefab;
    static GameObject satellitePrefabStatic;

    public static bool planetsLoaded;

    public Material lineMaterial;
    public static Material lineMaterialStatic;

    public static Planet GetCurrentPlanetFocus
    {
        get
        {
            if (!GameController.IsCloseView)
                return null;

            return systemPlanets[planetIndex];
        }
    }

    public static Planet SetCurrentPlanetFocus
    {
        set
        {
            for (int i = 0; i < systemPlanets.Count; i++)
            {
                if (value == systemPlanets[i])
                {
                    planetIndex = i;
                    //GameController.cameraMode = GameController.CameraMode.close;
                }
            }
        }
    }

    public static Vector3 GetCurrentPlanetFocusPosition
    {
        get
        {
            return systemPlanets[planetIndex].gameObject.transform.position;
        }
    }

    static int planetIndex = 0;
    void Start()
    {
        satellitePrefabStatic = satellitePrefab;
        lineMaterialStatic = lineMaterial;
        //left.onClick.AddListener(delegate { ChangeFocus(-1); });
        //right.onClick.AddListener(delegate { ChangeFocus(1); });
        //addSatellite.onClick.AddListener(delegate { AddSatellite(satellitePrefabs[0]); });
        SolarSystemManager.onChangeSystemFocus.AddListener(OnChangeSystemFocus);
        //GetAllPlanets();
        planetsLoaded = true;
        OnChangeSystemFocus();
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer >= TICK_TIME_MS)
        {
            timer = 0;
            UpdatePlanets();
        }

       //left.interactable = (planetIndex != 0);
       //right.interactable = (planetIndex != (planets.Count - 1));
    }

    // public static void ChangeFocus(int value)
    // {
    //     planetIndex += value;
    //     planetIndex = Mathf.Clamp(planetIndex, 0, systemPlanets.Count - 1);
    // }

    // void GetAllPlanets()
    // {
    //     // // // // for (int i = 0; i < SolarSystemManager.solarSystemManagers.Count; i++)
    //     // // // //     planets.AddRange(SolarSystemManager.solarSystemManagers[i].SystemPlanets);

    //     // for (int i = 0; i < transform.childCount; i++)    //OLD
    //     // {
    //     //     if (transform.GetChild(i).GetComponent<Planet>() != null)
    //     //         planets.Add(transform.GetChild(i).GetComponent<Planet>());
    //     // }
    // }

    void UpdatePlanets()
    {
        for (int i = 0; i < planets.Count; i++)
        {
            if (planets[i] != null)
                planets[i].UpdatePlanet();
            else
            {
                Debug.Log("Planet ref null, removing");
                planets.RemoveAt(i);
                i--;
            }
        }
    }

    public static void AddSatellite(Satellite satelliteType)
    {
        GameObject satellite = Instantiate(satellitePrefabStatic, GetCurrentPlanetFocusPosition, Quaternion.identity);
        satellite.transform.parent = GetCurrentPlanetFocus.gameObject.transform;
        satellite.GetComponent<SatelliteBehaviour>().attachedPlanet = GetCurrentPlanetFocus;
        satellite.GetComponent<SatelliteBehaviour>().RandomizeRotation();
        satellite.GetComponent<SatelliteBehaviour>().Set(satelliteType);
        GetCurrentPlanetFocus.satellites.Add(satellite.GetComponent<SatelliteBehaviour>());
    }

    public static float GetPlanetsHorizontalDistance(out Vector3 midpoint)
    {
        //if (planets.Count == 0)/////////////////////////////////////////////////////////////////////////REPLACE?
            //Debug.LogError("No planets, cannot position camera");

        Vector3 min = Vector3.zero, max = Vector3.zero;

        // for (int i = 0; i < planets.Count; i++) //OLD
        // {
        //     Vector3 position = planets[i].gameObject.transform.position;
        //     if (position.x <= min.x)
        //         min.x = position.x;
        //     if (position.x >= max.x)
        //         max.x = position.x;
        // }

        // midpoint = (min + max) / 2;
        // float absTotal = Mathf.Abs(min.x) + max.x;
        // return absTotal * FAR_VIEW_MULTIPLIER;

        midpoint = SolarSystemManager.SystemInFocus.transform.position;

        return SolarSystemManager.SystemInFocus.DistanceOfFurthestPlanet * 2 * FAR_VIEW_MULTIPLIER;
    }

    string AdverageValue(float value)
    {
        if (averageValue == 0)
            averageValue = value;
        else
            averageValue = (averageValue + value) / 2;

        return averageValue.ToString();
    }

    void OnChangeSystemFocus()
    {
        if (SolarSystemManager.SystemInFocus != null)
            systemPlanets = SolarSystemManager.SystemInFocus.SystemPlanets;
        planetIndex = 0;
    }

    // void UpdateLineRenderer()
    // {
    //     for (int i = 0; i < planets.Count; i++)
    //     {
    //         for (int j = 0; j < planets[i].connections.Count; j++)
    //         {
    //             DrawConnection(planets[i].transform.position, planets[i].connections[j].transform.position);
    //         }
    //     }
    // }

    // void DrawConnection(Vector3 start, Vector3 end)
    // {
    //     if (GameController.IsCloseView)
    //     {
    //         lineRenderer.enabled = false;
    //         return;

    //     GameObject lineObject = new GameObject("Line Renderer Object");
    //     lineObject.transform.SetParent(this.transform);
    //     LineRenderer lineRenderer = lineObject.AddComponent<LineRenderer>();

    //     //lineRenderer.enabled = false;
    //     lineRenderer.startWidth = LINE_THICKNESS;
    //     lineRenderer.endWidth = LINE_THICKNESS;
 

    //     lineRenderer.enabled = true;
    //     lineRenderer.SetPosition(0, Vector3.Lerp(start, end, LINE_LERP_THRESHHOLD));
    //     lineRenderer.SetPosition(1, Vector3.Lerp(start, end, 1 - LINE_LERP_THRESHHOLD));
    // }

    // void DrawLines()
    // {
    //     GameObject lineObject = new GameObject("Line Renderer Object");
    //     lineObject.transform.SetParent(this.transform);
    //     LineRenderer lineRenderer = lineObject.AddComponent<LineRenderer>();
        
    //     for (int i = 0; i < (planets.Count - 1); i++)
    //     {
    //     lineRenderer.SetPosition(0, this.transform.position);

    //     Debug.Log("DrawLinesCalled");

    //     lineRenderer.SetPosition(1, parentNode[i].gameObject.transform.position);
    //     lineRenderer.startWidth = 0.04f;
    //     lineRenderer.endWidth = 0.04f;
            
    //     }

    //     //lineRenderer.material = inactiveMaterial;
    // }
}