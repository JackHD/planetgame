﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GalaxyManager : MonoBehaviour
{
    public static List<SolarSystemManager> galaxySolarSystems = new List<SolarSystemManager>();
    //List<GalaxyManager> galaxyManagers = new List<GalaxyManager>();

    const float GRID_SIZE_X = 170f,
                GRID_SIZE_Y = 160f,
                ODD_ROW_OFFSET = 70f;
    const int GRID_COUNT_X = 5,
              GRID_COUNT_Y = 6,
              AVERAGE_SOLAR_SYSTEMS = 5,
              SOLAR_SYSTEM_COUNT_VARIANCE = 5;

    const int AVERAGE_SPACING = (GRID_COUNT_X * GRID_COUNT_Y) / AVERAGE_SOLAR_SYSTEMS;

    public static GalaxyManager currentGalaxyManager;

    public static string galaxyName = "Name";

    // Start is called before the first frame update
    void Start()
    {
        currentGalaxyManager = this;
        CreateNewGalaxy();
        CameraController.Recentre();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static void CreateNewGalaxy()
    {
        DestroyExistingGalaxy();

        int solarSystemsAdded = 0,
            nextSpacing = GetNewSpacing();

        List<GameObject> newSolarSystems = new List<GameObject>();

        float gridSizeX = Screen.width / GRID_COUNT_X;
        float gridSizeY = Screen.height / GRID_COUNT_Y;
        
        for (int x = 0; x < GRID_COUNT_X; x++)
        {
            for (int y = 0; y < GRID_COUNT_Y; y++)
            {
                int currentSpacing = x * y;
                //Debug.Log(currentSpacing + " " + averageSpacing);

                if (currentSpacing >= nextSpacing)
                {
                    bool oddRow = (y % 2) == 0;
                    float offset = 0;
                    if (oddRow)
                        offset = ODD_ROW_OFFSET;
                    solarSystemsAdded++;

                    //add solar system
                    
                    Vector3 newPosition = new Vector3(GRID_SIZE_X * x, GRID_SIZE_Y * y, 0);
                    //Debug.Log("Adding solar system to galaxy. Spacing: " + nextSpacing + " pos: " + newPosition + " odd row " + oddRow);
                    


                    GameObject newSolarSystem = Instantiate(GameController.SolarSystemPrefab);
                    newSolarSystem.transform.SetParent(currentGalaxyManager.transform);
                    newSolarSystem.transform.localPosition = newPosition + new Vector3(offset, 0, 0);
                    newSolarSystems.Add(newSolarSystem);
                    galaxySolarSystems.Add(newSolarSystem.GetComponent<SolarSystemManager>());
                    nextSpacing = GetNewSpacing();

                    if (solarSystemsAdded >= AVERAGE_SOLAR_SYSTEMS)
                        goto END;
                }
            }
        }
        END:
        ConfigureConnections(ref newSolarSystems);
        CameraController.Recentre();
    }

    static int GetNewSpacing()
    {
        return AVERAGE_SPACING + Random.Range(-SOLAR_SYSTEM_COUNT_VARIANCE, SOLAR_SYSTEM_COUNT_VARIANCE);
    }

    static void ConfigureConnections(ref List<GameObject> solarSystems)
    {
        for (int i = 1; i < solarSystems.Count; i++)
        {
            //Debug.Log("Adding connection between " + solarSystems[i].name + " and " + solarSystems[i-1].name);
            solarSystems[i].GetComponent<SolarSystemManager>().connections.Add(solarSystems[i-1]);
        }

        if (Random.Range(0, 10) <= 6)
            solarSystems[0].GetComponent<SolarSystemManager>().connections.Add(solarSystems[solarSystems.Count - 1]);
    }

    static void DestroyExistingGalaxy()
    {
        //clear refs
        PlanetManager.planets.Clear();
        SolarSystemManager.solarSystemManagers.Clear();
        galaxySolarSystems.Clear();

        //remove children
        for (int i = 0; i < currentGalaxyManager.gameObject.transform.childCount; i++)
            Destroy(currentGalaxyManager.gameObject.transform.GetChild(i).gameObject);
    }

    public static int TotalPopulation
    {
        get
        {
            int returnValue = 0;
            for (int i = 0; i < galaxySolarSystems.Count; i++)
            {
                returnValue += galaxySolarSystems[i].TotalPopulation;
            }
            return returnValue;
        }
    }
}
