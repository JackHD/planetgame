﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemPanelBehaviour : MonoBehaviour
{
    public Upgrade upgrade;
    public Satellite satellite;
    public Text itemName, description, cost;
    public Image image;

    public Button button;

    public float Height
    { get { return GetComponent<RectTransform>().sizeDelta.y; } }

    public void Set()
    {
        if (upgrade != null)
        {
            itemName.text = upgrade.upgradeName;
            description.text = upgrade.description;
            if (upgrade.cost == 0)
                cost.text = "Free";
            else
                cost.text = upgrade.cost.ToString();
            image.sprite = upgrade.sprite;
            button.onClick.AddListener(Activate);
        }

        if (satellite != null)
        {
            itemName.text = satellite.satelliteName;
            description.text = satellite.description;
            
            if (satellite.cost == 0)
                cost.text = "Free";
            else
                cost.text = satellite.cost.ToString();

            image.sprite = satellite.sprite;
            button.onClick.AddListener(Activate);
        }
    }

    public void OnUpdate()
    {
        if (upgrade != null)
        {
            button.interactable = PlayerController.Money >= upgrade.cost;
            button.transform.GetChild(0).gameObject.SetActive(PlayerController.Money >= upgrade.cost);
        }
        if (satellite != null)
            button.interactable = PlayerController.Money >= satellite.cost;
    }

    public void Activate()
    {
        if (upgrade != null && AttemptPurchase(upgrade.cost))
            upgrade.Activate();
        if (satellite != null && AttemptPurchase(satellite.cost))
            satellite.Add();
    }

    public static bool AttemptPurchase(int amount)
    {
        if (PlayerController.Money >= amount)
        {
            PlayerController.AddMoney = -amount;
            return true;
        }
        return false;
    }
}
