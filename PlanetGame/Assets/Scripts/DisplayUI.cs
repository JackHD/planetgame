﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIElement : MonoBehaviour
{
    public Text text;

    public string prefix, suffix;

    void Update()
    {
        text.text = prefix + PlayerController.Money + suffix;
    }
}
