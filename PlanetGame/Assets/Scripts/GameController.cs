﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class GameController : MonoBehaviour
{
    public List<Upgrade> upgrades;
    public List<Satellite> satellites;

    public static GameController gameController;

    public Button zoomInButton, zoomOutButton;

    public Image flashEffect;

    public static Vector3 lastInput = Vector3.zero;

    public GameObject mainCanvas, planetPrefab, solarSystemPrefab, ascendingTextPrefab;
    public static bool buyMenuIsOpen = false, infoMenuIsOpen = false;

    public static bool MenuIsOpen
    { get { return buyMenuIsOpen || infoMenuIsOpen; }}
    const float FLASH_FADE_SPEED = 1,
                FLASH_HIDE_THRESHHOLD = 0.01f; //min alpha before flash dissapears

    public static bool IsCloseView
    { get { return cameraMode == CameraMode.close; } }

    public static bool IsSystemView
    { get { return cameraMode == CameraMode.system; } }

    public static bool IsGalaxyView
    { get { return cameraMode == CameraMode.galaxy; } }

    public static bool IsMouseOverUI
    { get { return EventSystem.current.IsPointerOverGameObject(); } }

    public static UnityEvent onViewChange = new UnityEvent();

    public enum GameState { start, run, paused }
    public enum CameraMode { close, system, galaxy }
    public static CameraMode cameraMode = CameraMode.close;

    public static List<Upgrade> Upgrades
    { get { return gameController.upgrades; } }

    public static List<Satellite> Satellites
    { get { return gameController.satellites; } }

    public static GameObject PlanetPrefab
    { get { return gameController.planetPrefab; } }

    public static GameObject SolarSystemPrefab
    { get { return gameController.solarSystemPrefab; } }
    
    void Awake()
    {
        gameController = this;

        zoomInButton.onClick.AddListener( delegate { Zoom(-1); } );
        zoomOutButton.onClick.AddListener( delegate { Zoom(1); } );
        //leftButton.onClick.AddListener( delegate { NavigationButtonPressed(1); } );
        //rightButton.onClick.AddListener( delegate { NavigationButtonPressed(-1); } );
    }
    void Start()
    {
        cameraMode = CameraMode.galaxy;
        TriggerFadeIn();
        //SolarSystemManager.CycleSystems(1);
        //cameraButton.onClick.AddListener( ToggleZoom );
        if (onViewChange != null)
            onViewChange.Invoke();

        if (SolarSystemManager.onChangeSystemFocus != null)
            SolarSystemManager.onChangeSystemFocus.Invoke();
            else Debug.Log("invoke failed in game controller");

    }

    // Update is called once per frame
    void Update()
    {
        if (flashEffect.color.a <= FLASH_HIDE_THRESHHOLD)
            flashEffect.color = Planet.SetColourAlpha(flashEffect.color, 0);
        else
            flashEffect.color = Planet.SetColourAlpha(flashEffect.color, flashEffect.color.a - (Time.deltaTime * FLASH_FADE_SPEED));

        if (Input.touchCount != 0)
            lastInput = Input.GetTouch(0).position;
        else
            lastInput = Input.mousePosition;
    }

    public static void Zoom(int direction)
    {
        if (direction > 0)
            cameraMode++;
        else
            cameraMode--;

        if ((int)cameraMode > 2)
            cameraMode = GameController.CameraMode.galaxy;

        if ((int)cameraMode < 0)
            cameraMode = GameController.CameraMode.close;

        if (onViewChange != null)
            onViewChange.Invoke();
    }

    // public static void ToggleZoom()
    // {

    //     if (cameraMode == CameraMode.close)
    //         cameraMode = CameraMode.system;
    //     else if (cameraMode == CameraMode.system)
    //         cameraMode = CameraMode.galaxy;
    //     else if (cameraMode == CameraMode.galaxy)
    //         cameraMode = CameraMode.close;

    //     Debug.Log("Viewmode: " + cameraMode.ToString());
    //     // cameraMode = WrapClamp(cameraMode, 0, 2);

    //     // if (cameraMode == CameraMode.close)
    //     //     cameraMode = CameraMode.far;
    //     // else
    //     //     cameraMode = CameraMode.close;

    //     // if (onViewChange != null)
    //     //     onViewChange.Invoke();
    //     // else
    //     //     Debug.Log("onViewChange null");
    // }

    public void TriggerFlash()
    {
        flashEffect.color = Color.white;
        flashEffect.color = Planet.SetColourAlpha(flashEffect.color, 1);
    }

    public void TriggerFadeIn()
    {
        flashEffect.color = Color.black;
        flashEffect.color = Planet.SetColourAlpha(flashEffect.color, 1);
    }

    public static int WrapClamp(int value, int min, int max)
    {
        if (value > max)
            return min;
        if (value < min)
            return max;

        return value;
    }

    // void NavigationButtonPressed(int direction)
    // {
    //     if (IsGalaxyView)
    //         SolarSystemManager.CycleSystems(-direction);
    //     else
    //         PlanetManager.ChangeFocus(direction);
    // }

    public static string FormatNumber(int number)
    {
         string returnValue;

         if (number >= 1000000)
             returnValue = Mathf.Floor(number / 1000000).ToString() + "M";
         else if (number >= 1000)
             returnValue = Mathf.Floor(number / 1000).ToString() + "K";
         else
             returnValue = number.ToString();

         return returnValue;
    }

    public static void CloseMenus()
    {
        BuyPanelController.buyPanelController.isOpen = false;
        InfoPanelController.infoPanelController.isOpen = false;
    }

    public static string GetCurrectFocusObjectName()
    {
        if (IsCloseView)
            return PlanetManager.GetCurrentPlanetFocus._planetName;

        if (IsSystemView)
            return SolarSystemManager.SystemInFocus.solarSystemName;

        if (IsGalaxyView)
            return GalaxyManager.galaxyName;

        return "Error: No view mode.";
    }

    public static int GetCurrectFocusObjectPopulation()
    {
        if (IsCloseView)
            return PlanetManager.GetCurrentPlanetFocus._population;

        if (IsSystemView)
            return SolarSystemManager.SystemInFocus.TotalPopulation;

        if (IsGalaxyView)
            return GalaxyManager.TotalPopulation;

        return 0;
    }

    public static string ZoomLevelName
    {
        get
        {
            if (IsSystemView)
                return "Solar System";
            if (IsGalaxyView)
                return "Galaxy";
            return "Planet";
        }
    }

    public static void SpawnAscendingText(string text, Vector3 startPosition)
    {
        if (!IsCloseView)
            return;

        GameObject ascendingText = Instantiate(gameController.ascendingTextPrefab, startPosition, Quaternion.identity);
        ascendingText.transform.SetParent(gameController.mainCanvas.transform);
        ascendingText.GetComponent<AscendingText>().textToDisplay = text;
    }
}
