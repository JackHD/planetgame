﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public Vector3 positionOffset, galaxyViewOffset;

    bool gotPlanetCentre = false;
    Vector3 planetCentre = Vector3.zero, galaxyCentre = Vector3.zero;
    float closeViewSize = 5, farViewSize, galaxyViewSize, verticalDistance;

    public float lerpSpeed;

    Camera cam;

    static CameraController cameraController;

    // Start is called before the first frame update
    void Start()
    {
        cameraController = this;
        cam = GetComponent<Camera>();
        //initialSize = cam.orthographicSize;
        SolarSystemManager.onChangeSystemFocus.AddListener( OnSystemChange );
    }

    // Update is called once per frame
    void Update()
    {
        if (!PlanetManager.planetsLoaded) //wait for planetmanager to find all planets
            return;

        if (!gotPlanetCentre)
        {
            gotPlanetCentre = true;

            farViewSize = PlanetManager.GetPlanetsHorizontalDistance(out planetCentre);
            galaxyViewSize = SolarSystemManager.GetSystemsHorizontalDistance(out galaxyCentre, out verticalDistance);
        }

        Vector3 targetPosition = Vector3.zero;
        float targetSize = closeViewSize;

        switch (GameController.cameraMode)
        {
            case GameController.CameraMode.close:
                targetPosition = PlanetManager.GetCurrentPlanetFocusPosition;
                break;
            case GameController.CameraMode.system:
                //targetPosition = planetCentre;
                targetPosition = SolarSystemManager.SystemInFocus.transform.position;
                targetSize = farViewSize;
                break;
            case GameController.CameraMode.galaxy:
                targetPosition = galaxyCentre + galaxyViewOffset;
                targetSize = galaxyViewSize + (verticalDistance / 10);
                break;
            default:
            break;
        }

        targetPosition += positionOffset;

        cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, targetSize, lerpSpeed * Time.deltaTime * 0.8f);
        transform.position = Vector3.Lerp(transform.position, targetPosition, lerpSpeed * Time.deltaTime);
    }

    void OnSystemChange()
    {
        gotPlanetCentre = false;
    }

    public static void Recentre()
    {
        cameraController.gotPlanetCentre = false;
    }
}
