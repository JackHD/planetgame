﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetGenerator : MonoBehaviour
{

    [System.Serializable]
    public class SpritePool
    {
        public bool overrideShading = false;
        public float chanceToAppear = 1;
        [SerializeField]
        public List<Sprite> spritePool = new List<Sprite>();
    }

    [System.Serializable]
    public class SingleLayerPlanet
    {
        public float chanceToAppear = 0.05f;
        public Sprite planetSprite;
    }

    public List<SpritePool> sprites = new List<SpritePool>();
    static List<SpritePool> spritesStatic = new List<SpritePool>();

    public List<SingleLayerPlanet> singleLayerPlanets = new List<SingleLayerPlanet>();
    static List<SingleLayerPlanet> singleLayerPlanetsStatic = new List<SingleLayerPlanet>();

    // Start is called before the first frame update
    void Awake()
    {
        spritesStatic = sprites;
        singleLayerPlanetsStatic = singleLayerPlanets;
    }

    public static List<Planet.PlanetLayer> GetPlanet()
    {
        List<Planet.PlanetLayer> returnList = new List<Planet.PlanetLayer>();

        for (int i = 0; i < singleLayerPlanetsStatic.Count; i++)
        {
            if (singleLayerPlanetsStatic[i].chanceToAppear >= Random.Range(0f, 1f))
            {
                returnList.Add(new Planet.PlanetLayer(0, false, singleLayerPlanetsStatic[i].planetSprite));
                return returnList;
            }
        }

        for (int i = 0; i < spritesStatic.Count; i++)
        {
            if (spritesStatic[i].chanceToAppear >= Random.Range(0f, 1f))
                returnList.Add(new Planet.PlanetLayer(GetRandomRotation(), spritesStatic[i].overrideShading, GetRandomSprite(spritesStatic[i])));
        }
        return returnList;
    }

    static int GetRandomRotation()
    {
        int returnValue = Random.Range(0, 3);
        return returnValue * 90;
    }

    static Sprite GetRandomSprite(SpritePool input)
    {
        int selection = Random.Range(0, input.spritePool.Count);
        return input.spritePool[selection];
    }
}
