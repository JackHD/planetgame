﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Planet : MonoBehaviour
{
    public string _planetName;
    public int _population;
    int _initialPopulation;
    float _initialColliderRadius;
    public float _populationRate;
    public bool _destroyed;

    public Text nameText, nameTextLarge, populationText, populationTextLarge;

    public SpriteRenderer image;

    public CircleCollider2D planetCollider;

    public Button destroyButton;

    public PlanetRenderer planetRenderer;

    public TrailRenderer trailRenderer;

    public GameObject overlay;

    public List<SatelliteBehaviour> satellites = new List<SatelliteBehaviour>();

    public List<GameObject> connections = new List<GameObject>();
    List<LineRenderer> lineRenderers = new List<LineRenderer>();

    const float RING_COLOUR_MAX_ALPHA = 0.25f,
                LINE_THICKNESS = 0.45f,
                LINE_LERP_THRESHHOLD_MULTIPLIER = 4.5f;

    public bool PopulationDepleted
    { get { return _population <= 0; } }

    public class PlanetLayer
    {
        public int rotation;
        public Sprite sprite;
        public bool overrideShading = false;
        public PlanetLayer(int rotation, bool overrideShading, Sprite sprite)
        {
            this.rotation = rotation;
            this.overrideShading = overrideShading;
            this.sprite = sprite;
        }
    }

    public List<PlanetLayer> planetLayers;

    void Start()
    {
        PlanetManager.planets.Add(this);
        _initialPopulation = _population;
        _destroyed = false;
        destroyButton.onClick.AddListener( delegate { Destroyed(); });
        InitializeConnections();
        GameController.onViewChange.AddListener(ResizeCollider);
        _initialColliderRadius = planetCollider.radius;
        planetRenderer.attachedPlanet = this;
        Render();
    }

    public void Damage(int amount, bool isSatallite)
    {
        if (amount == 0)
            return;

        int newPopulation = _population - amount;
        //_population -= amount;
        if (newPopulation < 0)
        {
            newPopulation = 0;
            PlayerController.AddMoney = _population;

            if (isSatallite)
                GameController.SpawnAscendingText("-" + _population.ToString(), Camera.main.WorldToScreenPoint(transform.position));
            else
                GameController.SpawnAscendingText("-" + _population.ToString(), GameController.lastInput);
        }
        else
        {
            PlayerController.AddMoney = amount;
            //GameController.SpawnAscendingText("-" + amount.ToString(), GameController.lastInput);

            if (isSatallite)
                GameController.SpawnAscendingText("-" + amount.ToString(), Camera.main.WorldToScreenPoint(transform.position));
            else
                GameController.SpawnAscendingText("-" + amount.ToString(), GameController.lastInput);
        }
        

        _population = newPopulation;

        // if (_population < 0)
        //     _population = 0;
    }

    void Destroyed()
    {
        _destroyed = true;
        _population = 0;
        populationText.text = "Destroyed";
        PlayerController.PlanetsDestroyed++;
    }

    public void UpdatePlanet()
    {
        UpdateSatellites();
        UpdateConnections();

        // gameObject.SetActive(GameController.cameraMode == GameController.CameraMode.system
        //    || GameController.cameraMode == GameController.CameraMode.close);

        // if (GameController.IsGalaxyView)
        //     gameObject.SetActive(false);
        // else if (GameController.IsCloseView)
        //     gameObject.SetActive(PlanetManager.GetCurrentPlanetFocus == this);
        // else if (GameController.IsSystemView)
        //     gameObject.SetActive(true);

        switch(GameController.cameraMode)
        {
            case GameController.CameraMode.close:
                gameObject.SetActive(PlanetManager.GetCurrentPlanetFocus == this);
                break;
            case GameController.CameraMode.system:
                gameObject.SetActive(true);
                break;
            case GameController.CameraMode.galaxy:
                gameObject.SetActive(false);
                break;
        }

        nameText.text = _planetName;
        nameTextLarge.text = _planetName;

        // if (_destroyed)
        //     populationTextLarge.text = GameController.FormatNumber(_population);

        nameText.enabled = GameController.IsCloseView;
        populationText.enabled = GameController.IsCloseView;
        nameTextLarge.enabled = !GameController.IsCloseView;
        populationTextLarge.enabled = !GameController.IsCloseView;

        //ring.enabled = (PlanetManager.GetCurrentPlanetFocus == this);// && !GameController.IsCloseView;

        if (GameController.cameraMode == GameController.CameraMode.system)
        {
            trailRenderer.enabled = true;
            //trailRenderer.emitting = true;
            //trailRenderer.SetActive(true);
        }
        else
        {
            //trailRenderer.SetActive(false);
            trailRenderer.enabled = false;
            //trailRenderer.emitting = false;
            trailRenderer.Clear();
        }
        //trailRenderer.SetActive(!((PlanetManager.GetCurrentPlanetFocus == this) && GameController.IsCloseView));
      
        float targetAlpha;

        if (GameController.IsCloseView)
            targetAlpha = 0;
        else
            targetAlpha = RING_COLOUR_MAX_ALPHA * Mathf.Abs(Mathf.Sin(Time.time));

        //ring.color = SetColourAlpha(ring.color, Mathf.Lerp(ring.color.a, targetAlpha, Time.deltaTime * 15));

        if (!_destroyed)
        {

            if (!PopulationDepleted)
            {


                // float newPopulation = (float)_population * (float)_populationRate; //REENABLE!!!
                // _population = (int)newPopulation;
                _population = CalculateNewPopulation(1, _population, _populationRate);

                populationText.text = "Population: " + _population.ToString();
                //populationTextLarge.text = "Population: " + _population.ToString();
                populationTextLarge.text = GameController.FormatNumber(_population);
            }

            destroyButton.gameObject.SetActive(PopulationDepleted);
            
            if (PopulationDepleted)
            {
                populationText.text = "Population: 0";
                //populationTextLarge.text = "Population: 0";
                populationTextLarge.text = "0";
            }
        }
        else
        {
            image.color = new Color(0.5f, 0.1f, 0.1f);
            destroyButton.gameObject.SetActive(false);
            populationText.text = "Destroyed";
            populationTextLarge.text = "Destroyed";
        }

        overlay.SetActive(!planetRenderer.isSingleLayer);
        overlay.transform.rotation = Quaternion.LookRotation(SolarSystemManager.SystemInFocus.transform.position - transform.position, Vector3.back);
    }

    int CalculateNewPopulation(float x, int a, float r)
    {
        // Logistic growth formula:
        // f(x) = a(1 + r)^x
        // where: x = time interval, a = initial amount, r = growth rate

        float returnValue = (float)a*(1 + r);
        //returnValue = Mathf.Pow(returnValue, x);
        return (int)returnValue;
    }

    void UpdateSatellites()
    {
        for (int i = 0; i < satellites.Count; i++)
            satellites[i].UpdateSatellite();
    }

    void OnMouseDown()
    {
        //Debug.Log("OnMouseDown called for planet.");
        if (GameController.MenuIsOpen)
            return;

        if (GameController.IsMouseOverUI)
            return;

        if (SolarSystemManager.onChangeSystemFocus != null)
            SolarSystemManager.onChangeSystemFocus.Invoke();

        if (PlanetManager.GetCurrentPlanetFocus != this)
            PlanetManager.SetCurrentPlanetFocus = this;

        if (GameController.IsCloseView)
        {
            Damage(PlayerController.DamagePerClick, false);
        }
        else
            GameController.Zoom(-1);
    }

    public static Color SetColourAlpha(Color colour, float alpha)
    {
        return new Color(colour.r, colour.g, colour.b, alpha);
    }

    void InitializeConnections()
    {
        for (int i = 0; i < connections.Count; i++)
        {
            GameObject lineObject = new GameObject("Line Renderer Object");
            lineObject.transform.SetParent(this.transform);
            LineRenderer lineRenderer = lineObject.AddComponent<LineRenderer>();
            lineRenderer.startWidth = LINE_THICKNESS;
            lineRenderer.endWidth = LINE_THICKNESS;
            lineRenderer.enabled = false;
            
            float adjustedLerpThreshhold = LINE_LERP_THRESHHOLD_MULTIPLIER * (1 / Vector3.Distance(transform.position, connections[i].transform.position));
            //Debug.Log(adjustedLerpThreshhold);

            lineRenderer.SetPosition(0, Vector3.Lerp(transform.position, connections[i].transform.position, adjustedLerpThreshhold));
            lineRenderer.SetPosition(1, Vector3.Lerp(transform.position, connections[i].transform.position, 1 - adjustedLerpThreshhold));
            lineRenderers.Add(lineRenderer);
        }
    }

    void UpdateConnections()
    {
        for (int i = 0; i < lineRenderers.Count; i++)
        {
            lineRenderers[i].material = PlanetManager.lineMaterialStatic;
            lineRenderers[i].enabled = !GameController.IsCloseView;
        }
    }

    void ResizeCollider()
    {
        //Debug.Log("Changing planet collider size");
        if (GameController.IsCloseView)
            planetCollider.radius = _initialColliderRadius;
        else
            planetCollider.radius = _initialColliderRadius * 15;
    }

    public void Render()
    {
        planetRenderer.RenderLayers(PlanetGenerator.GetPlanet());
    }
}

