﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class BuyPanelController : MonoBehaviour
{
    public bool isOpen = false;

    public static bool isMouseOver = false;
    public float movementSpeed = 10f;

    public static BuyPanelController buyPanelController;

    RectTransform panelRect;
    public RectTransform buttonRect;
    public Button upgradeButton, satelliteButton, weaponButton;

    public GameObject upgradePanel, satellitePanel, weaponPanel, satellitePanelContent, satellitePanelNullMessage;
    Button button;

    public List<ItemPanelBehaviour> upgradeItems = new List<ItemPanelBehaviour>(),
                                    satelliteItems = new List<ItemPanelBehaviour>();

    public enum PanelMode { upgrade, satellite, weapon };
    PanelMode panelMode;

    public GameObject upgradeMenuItemPrefab;

    const float UPGRADE_ITEM_PANEL_Y_OFFSET = 10f;

    float Width
    { 
        get
        {
            return panelRect.sizeDelta.x;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        //buttonRect.gameObject.GetComponent<Button>();
        buyPanelController = this;
        panelRect = GetComponent<RectTransform>();
        button =  buttonRect.gameObject.GetComponent<Button>();
        button.onClick.AddListener( delegate { Toggle(); } );
        panelMode = PanelMode.upgrade;
        upgradeButton.onClick.AddListener( delegate { panelMode = PanelMode.upgrade; } );
        satelliteButton.onClick.AddListener( delegate { panelMode = PanelMode.satellite; } );
        weaponButton.onClick.AddListener( delegate { panelMode = PanelMode.weapon; } );

        GetUpgrades();
        GetSatellites();
    }

    // Update is called once per frame
    void Update()
    {
        GameController.buyMenuIsOpen = isOpen;
        if (isOpen)
        {
            LerpPanelToPosition(new Vector3(panelRect.sizeDelta.x, 0));
            LerpButtonToPosition(new Vector3(-buttonRect.sizeDelta.x, 0));
        }
        else
        {
            LerpPanelToPosition(new Vector3(0, 0));
            LerpButtonToPosition(new Vector3(0, 0));
        }
        UpdatePanelMode();
        UpdateMenuItems();
        isMouseOver = EventSystem.current.IsPointerOverGameObject();
    }

    void Toggle()
    {
        if (isOpen)
            GameController.CloseMenus();
        else
        {
            GameController.CloseMenus();
            isOpen = true;;
        }
    }

    void LerpPanelToPosition(Vector3 targetPosition)
    {
        panelRect.anchoredPosition = Vector3.Lerp(panelRect.anchoredPosition, targetPosition, Time.deltaTime * movementSpeed);
    }

    void LerpButtonToPosition(Vector3 targetPosition)
    {
        buttonRect.anchoredPosition = Vector3.Lerp(buttonRect.anchoredPosition, targetPosition, Time.deltaTime * movementSpeed * 0.7f);
    }

    void UpdatePanelMode()
    {
        upgradeButton.interactable = panelMode != PanelMode.upgrade;
        satelliteButton.interactable = panelMode != PanelMode.satellite;
        weaponButton.interactable = panelMode != PanelMode.weapon;

        upgradePanel.SetActive(panelMode == PanelMode.upgrade);
        satellitePanel.SetActive(panelMode == PanelMode.satellite);
        weaponPanel.SetActive(panelMode == PanelMode.weapon);

        if (satellitePanel.activeSelf)
        {
            satellitePanelContent.SetActive(GameController.IsCloseView);
            satellitePanelNullMessage.SetActive(!GameController.IsCloseView);
        }

        switch (panelMode)
        {
            case PanelMode.upgrade:
            return;
            case PanelMode.satellite:
            return;
            case PanelMode.weapon:
            return;
        }
    }

    void GetUpgrades()
    {
        for (int i = 0; i < GameController.Upgrades.Count; i++)
        {
            GameObject menuObject = Instantiate(upgradeMenuItemPrefab, Vector3.zero,  //instantiate menu prefab
                Quaternion.identity, upgradePanel.transform.GetChild(0).transform);

            RectTransform menuObjectRect = menuObject.GetComponent<RectTransform>(); //put in right place
            menuObjectRect.anchoredPosition = new Vector3(0, -menuObjectRect.sizeDelta.y * i, 0);

            menuObject.GetComponent<ItemPanelBehaviour>().upgrade = GameController.Upgrades[i]; //set UI values
            upgradeItems.Add(menuObject.GetComponent<ItemPanelBehaviour>());
            menuObject.GetComponent<ItemPanelBehaviour>().Set();
        }
    }

    void GetSatellites()
    {
        for (int i = 0; i < GameController.Satellites.Count; i++)
        {
            GameObject menuObject = Instantiate(upgradeMenuItemPrefab, Vector3.zero,  //instantiate menu prefab
                Quaternion.identity, satellitePanel.transform.GetChild(0).transform);

            RectTransform menuObjectRect = menuObject.GetComponent<RectTransform>(); //put in right place
            menuObjectRect.anchoredPosition = new Vector3(0, -menuObjectRect.sizeDelta.y * i, 0);

            menuObject.GetComponent<ItemPanelBehaviour>().satellite = GameController.Satellites[i]; //set UI values
            satelliteItems.Add(menuObject.GetComponent<ItemPanelBehaviour>());
            menuObject.GetComponent<ItemPanelBehaviour>().Set();
        }
    }

    void UpdateMenuItems()
    {
        for (int i = 0; i < upgradeItems.Count; i++)
            upgradeItems[i].OnUpdate();
        for (int i = 0; i < satelliteItems.Count; i++)
            satelliteItems[i].OnUpdate();
    }

    // void SetPanelMode(PanelMode mode)
    // {
    //     switch (mode)
    //     {
    //         case PanelMode.upgrade:
    //         return;
    //         case PanelMode.satellite:
    //         return;
    //         case PanelMode.weapon:
    //         return;
    //     }
    // }
}
