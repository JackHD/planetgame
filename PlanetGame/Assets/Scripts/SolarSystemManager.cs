﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SolarSystemManager : MonoBehaviour, IComparable<SolarSystemManager>
{
    List<Planet> planets = new List<Planet>();

    public const float ORBITAL_DISTANCE_INCREMENT = 18F,
                       ORBITAL_SPEED_DELTA = 80f;

    public static List<SolarSystemManager> solarSystemManagers = new List<SolarSystemManager>();
    public static SolarSystemManager SystemInFocus
    { 
        get 
        {
            if (solarSystemManagers.Count == 0)
            {
                systemInFocusIndex = 0;
                return null;
            }
            return solarSystemManagers[systemInFocusIndex];
        }
        set
        { 
            for (int i = 0; i < solarSystemManagers.Count; i++)
            {
                if (solarSystemManagers[i] == value)
                {
                    systemInFocusIndex = i;
                    if (onChangeSystemFocus != null)
                        onChangeSystemFocus.Invoke();

                    break;
                }
            }
        }
    }

    public static UnityEvent onChangeSystemFocus = new UnityEvent();

    public static int systemInFocusIndex = 0;

    public string solarSystemName = "Name";

    public List<GameObject> connections = new List<GameObject>();
    
    List<LineRenderer> lineRenderers = new List<LineRenderer>();

    public CircleCollider2D circleCollider;
    float circleColliderInitialRadius;

    public Text infoText;

    const float RING_COLOUR_MAX_ALPHA = 0.75f,
                LINE_THICKNESS = 5f,
                LINE_LERP_THRESHHOLD_MULTIPLIER = 4.5f;
    public List<Planet> SystemPlanets
    {
        get
        {
            List<Planet> returnList = new List<Planet>();
            for (int i = 0; i < orbitals.Count; i++)
                returnList.Add(orbitals[i].planet);

            return returnList;
        }
    }

    public float DistanceOfFurthestPlanet
    { get { return orbitals[orbitals.Count - 1].horizontalDistance; } }

    public static Planet LastPlanetInSystem
    { get { return SystemInFocus.SystemPlanets[SystemInFocus.SystemPlanets.Count - 1]; } }

    public int TotalPopulation
    {
        get
        {
            int returnValue = 0;
            for (int i = 0; i < SystemPlanets.Count; i++)
            {
                if (SystemPlanets[i] != null)
                    returnValue += SystemPlanets[i]._population;
            }

            return returnValue;
        }
    }

    public class Orbital
    {
        public float rotationSpeed = 40, horizontalDistance = 0;
        public GameObject rotationBase, planetBase;
        public Planet planet;
    }

    public List<Orbital> orbitals = new List<Orbital>();

    void Awake()
    {
        solarSystemManagers.Add(this);
        GetOrbitals();
    }

    // Start is called before the first frame update
    void Start()
    {
        AddPlanets();
        InitializeConnections();
        solarSystemManagers.Sort();
        circleColliderInitialRadius = circleCollider.radius;
        GameController.onViewChange.AddListener(delegate {OnViewChange();});
    }

    void Update()
    {
        OnUpdate();
    }

    public void OnUpdate()
    {
        UpdateConnections();
        //ring.enabled = GameController.IsGalaxyView && (SystemInFocus == this);
        for (int i = 0; i < orbitals.Count; i++)
        {
            float closeViewMultiplier = 0.5f;
            if (GameController.IsCloseView)
                closeViewMultiplier = 0.02f;

            orbitals[i].rotationBase.transform.Rotate(Vector3.forward, orbitals[i].rotationSpeed * Time.deltaTime * closeViewMultiplier);
            orbitals[i].planetBase.transform.rotation = Quaternion.identity;
        }

        if (!GameController.IsGalaxyView)
            gameObject.SetActive(SystemInFocus == this);

        infoText.enabled = GameController.IsGalaxyView;

        int planetsLeft = 0;
        for (int i = 0; i < planets.Count; i++)
        {
            if (!planets[i]._destroyed)
                planetsLeft++;
        }

        if (infoText.enabled)
            infoText.text = solarSystemName + "\nPlanets: " + planetsLeft + "\n" + GameController.FormatNumber(TotalPopulation) + " lives";
    }

    void GetOrbitals()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).gameObject.name.Contains("Orbital"))
            {
                Orbital newOrbital = new Orbital();
                newOrbital.rotationBase = transform.GetChild(i).gameObject;
                orbitals.Add(newOrbital);
            }
        }
    }

    void AddPlanets()
    {
        for (int i = 0; i < orbitals.Count; i++)
        {
            GameObject newPlanet = Instantiate(GameController.PlanetPrefab);
            newPlanet.transform.SetParent(orbitals[i].rotationBase.transform);
            orbitals[i].planetBase = newPlanet;
            orbitals[i].planet = newPlanet.GetComponent<Planet>();
            planets.Add(orbitals[i].planet);
            newPlanet.transform.localPosition = Vector3.right * ORBITAL_DISTANCE_INCREMENT * (i + 1);
            orbitals[i].rotationSpeed += UnityEngine.Random.Range(-ORBITAL_SPEED_DELTA, ORBITAL_SPEED_DELTA);
            if (Mathf.Abs(orbitals[i].rotationSpeed) < 10)
                orbitals[i].rotationSpeed *= 7;

            orbitals[i].rotationSpeed = Mathf.Clamp(orbitals[i].rotationSpeed, -ORBITAL_SPEED_DELTA, ORBITAL_SPEED_DELTA);
            orbitals[i].horizontalDistance = newPlanet.transform.localPosition.x;
        }
    }

    public static void CycleSystems(int direction)
    {
        systemInFocusIndex += direction;
        systemInFocusIndex = GameController.WrapClamp(systemInFocusIndex, 0, solarSystemManagers.Count - 1);

        //GameController.cameraMode = GameController.CameraMode.system;

        if (onChangeSystemFocus != null)
            onChangeSystemFocus.Invoke();
    }

    public static float GetSystemsHorizontalDistance(out Vector3 midpoint, out float verticalDistance)
    {
        if (solarSystemManagers.Count == 0)
            Debug.LogError("No systems, cannot position camera");

        Vector3 min = Vector3.zero, max = Vector3.zero;

        for (int i = 0; i < solarSystemManagers.Count; i++) //OLD
        {
            Vector3 position = solarSystemManagers[i].gameObject.transform.position;
            if (position.x <= min.x)
                min.x = position.x;
            if (position.x >= max.x)
                max.x = position.x;

            if (position.y <= min.y)
                min.y = position.y;
            if (position.y >= max.y)
                max.y = position.y;
        }

        // Vector3 averagePosition = solarSystemManagers[0].gameObject.transform.position;
        // for (int i = 1; i < solarSystemManagers.Count; i++)
        // {
        //     averagePosition += solarSystemManagers[i].gameObject.transform.position;
        //     averagePosition /= 2;
        // }
        // midpoint = averagePosition;
        midpoint = (min + max) / 2;
        float absTotal = Mathf.Abs(min.x) + max.x;
        verticalDistance = Mathf.Abs(min.y) + max.y;
        return absTotal * 1.5f;
    }

    void InitializeConnections()
    {
        for (int i = 0; i < connections.Count; i++)
        {
            GameObject lineObject = new GameObject("Line Renderer Object");
            lineObject.transform.SetParent(this.transform);
            LineRenderer lineRenderer = lineObject.AddComponent<LineRenderer>();
            lineRenderer.startWidth = LINE_THICKNESS;
            lineRenderer.endWidth = LINE_THICKNESS;
            lineRenderer.enabled = false;
            
            float adjustedLerpThreshhold = LINE_LERP_THRESHHOLD_MULTIPLIER * (1 / Vector3.Distance(transform.position, connections[i].transform.position));

            lineRenderer.SetPosition(0, Vector3.Lerp(transform.position, connections[i].transform.position, adjustedLerpThreshhold));
            lineRenderer.SetPosition(1, Vector3.Lerp(transform.position, connections[i].transform.position, 1 - adjustedLerpThreshhold));
            lineRenderers.Add(lineRenderer);
        }
    }

    void UpdateConnections()
    {
        for (int i = 0; i < lineRenderers.Count; i++)
        {
            lineRenderers[i].material = PlanetManager.lineMaterialStatic;
            lineRenderers[i].enabled = GameController.IsGalaxyView;
        }
    }

    public int CompareTo(SolarSystemManager other)
    {
        if (other == null)
            return 1;

        if (transform.position.x > other.transform.position.x)
            return 1;
        
        if (transform.position.x == other.transform.position.x)
            return 0;

        return -1;
    }

    void OnViewChange()
    {
        //Debug.Log("Changing system collider size");

        if (GameController.IsGalaxyView)
        {
            gameObject.SetActive(true);
            circleCollider.radius = circleColliderInitialRadius;
        }
        else
            circleCollider.radius = 0;
    }

    void OnMouseDown()
    {
        //Debug.Log("OnMouseDOwn called for solar sytem.");

        if (GameController.MenuIsOpen)
            return;

        if (BuyPanelController.isMouseOver)
            return;

        if (SystemInFocus != this)
            SystemInFocus = this;

        GameController.Zoom(-1);
    }
}
