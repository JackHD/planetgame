﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{

    static AudioSource audioSource;

    public List<AudioClip> songs = new List<AudioClip>();

    public static bool isMuted = false;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (!audioSource.isPlaying && !isMuted)
        {
            audioSource.clip = songs[Random.Range(0, songs.Count - 1)];
            audioSource.Play();

        }
    }

    public static void ToggleMute()
    {
        isMuted = !isMuted;

        if (isMuted)
            audioSource.Pause();
        else
            audioSource.Play();
    }
}
