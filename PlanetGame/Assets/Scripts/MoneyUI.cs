﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayUI : MonoBehaviour
{
    public Text text;

    public string prefix, suffix;

    public enum Function { money, moneyDelta, planetDelta, planetsDestroyed };
    public Function displayFunction;


    float tickTimer, averageValue, valueCache = 0;

    public bool usesTickTimer = false;

    void Update()
    {
        tickTimer += Time.deltaTime;

        if  (usesTickTimer)
        {
            if (!HasTimerElapsed())
                return;
        }

        string displayValue  = prefix;
        switch (displayFunction)
        {
            case Function.money:
                displayValue += PlayerController.Money.ToString();
                break;
            case Function.moneyDelta:
                displayValue += AdverageValue(PlayerController.Money - valueCache);
                valueCache = PlayerController.Money;
                break;
            case Function.planetsDestroyed:
                displayValue += PlayerController.PlanetsDestroyed.ToString();
            break;
            default:
                break;
        }
        displayValue += suffix;
        text.text = displayValue;
    }

    bool HasTimerElapsed()
    {
        if (tickTimer >= PlanetManager.TICK_TIME_MS)
        {
            tickTimer = 0;
            return true;
        }
        return false;
    }

    string AdverageValue(float value)
    {
        if (averageValue == 0)
            averageValue = value;
        else
            averageValue = (averageValue + value) / 2;

        return (averageValue * 10).ToString("00.00");
    }
}
