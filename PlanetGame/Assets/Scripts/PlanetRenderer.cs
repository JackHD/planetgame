﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetRenderer : MonoBehaviour
{
    public Planet attachedPlanet;
    public GameObject fragmentsParentObject, overlay;

    public List<Fragment> fragments = new List<Fragment>();

    public const float SPIN_MULTIPLIER = 7f,
                       DIRECTION_MULTIPLIER = 0.028f;

    public class Fragment
    {
        public Vector3 direction;
        public float spin;
        public GameObject gameObject;
        public SpriteRenderer sprite
        { get { return gameObject.GetComponent<SpriteRenderer>(); } }
        public Fragment(Vector3 direction, float spin, GameObject gameObject)
        {
            this.direction = direction;
            this.spin = spin;
            this.gameObject = gameObject;
        }
    }

    bool isDestroyed = false;

    public bool isSingleLayer = false;

    public void RenderLayers(List<Planet.PlanetLayer> input)
    {
        ClearChildren();

        for (int i = 0; i < input.Count; i++)
        {
            GameObject layerObject = new GameObject("LayerObject");
            layerObject.transform.SetParent(this.transform);
            layerObject.transform.position = transform.position;


            SpriteRenderer layerSprite = layerObject.AddComponent<SpriteRenderer>();


            if (input[i].overrideShading)
                layerSprite.sortingOrder = 50 + i;
            else
                layerSprite.sortingOrder = i;


            isSingleLayer = input.Count == 1;

            if (!isSingleLayer) //only do if not a single layer planet
            {
                layerObject.transform.Rotate(Vector3.forward, (float)input[i].rotation);
                layerSprite.color = GetRandomColour();
                layerSprite.flipX = Random.Range(0, 2) == 0;
                layerSprite.flipY = Random.Range(0, 2) == 0;
            }

            layerSprite.sprite = input[i].sprite;
        }
        GetFragmentRenderers();
        SetFragmentVisibility(false);
    }

    // public void RenderLayers(PlanetGenerator.SingleLayerPlanet input)
    // {
    //     ClearChildren();

    //     //for (int i = 0; i < input.Count; i++)
    //     {
    //         GameObject layerObject = new GameObject("LayerObject");
    //         layerObject.transform.SetParent(this.transform);
    //         layerObject.transform.position = transform.position;
    //         //layerObject.transform.Rotate(Vector3.forward, (float)input[i].rotation);
    //         SpriteRenderer layerSprite = layerObject.AddComponent<SpriteRenderer>();
    //         //layerSprite.sortingOrder = i;

    //         layerSprite.flipX = Random.Range(0, 2) == 0;
    //         layerSprite.flipY = Random.Range(0, 2) == 0;

    //         //layerSprite.color = GetRandomColour();

    //         layerSprite.sprite = input.planetSprite;
    //     }
    //     GetFragmentRenderers();
    //     SetFragmentVisibility(false);
    // }

    Color GetRandomColour()
    {
        return new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
    }

    void Update()
    {
        if ((attachedPlanet != null) && attachedPlanet._destroyed && !isDestroyed)
        {
            ClearChildren();
            SetFragmentVisibility(true);
            Destroy(overlay);
            GameController.gameController.TriggerFlash();
            isDestroyed = true;
        }

        if (isDestroyed)
        {
            for (int i = 0; i < fragments.Count; i++)
            {
                fragments[i].gameObject.transform.position += fragments[i].direction;
                fragments[i].direction *= 0.99f;
                fragments[i].gameObject.transform.Rotate(Vector3.forward, fragments[1].spin);
                fragments[i].spin *= 0.99f;
            }
        }
    }

    public void SetFragmentVisibility(bool visible)
    {
        for (int i = 0; i < fragments.Count; i++)
            fragments[i].sprite.enabled = visible;
    }

    void ClearChildren()
    {
        for (int i = 0; i < transform.childCount; i++) //clear children
        {
            if (!transform.GetChild(i).name.Contains("Fragment"))
                Destroy(transform.GetChild(i).gameObject);
        }
    }

    void GetFragmentRenderers()
    {
        for (int i = 0; i < fragmentsParentObject.transform.childCount; i++)
        {
            float spin = Random.Range(-1f, 1f) * SPIN_MULTIPLIER;
            Vector3 direction = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), 0) * DIRECTION_MULTIPLIER;
            fragments.Add(new Fragment(direction, spin, fragmentsParentObject.transform.GetChild(i).gameObject));
        }
    }
}
