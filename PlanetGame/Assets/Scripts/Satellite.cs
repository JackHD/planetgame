﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Satellite", menuName = "Satellite")]
public class Satellite : ScriptableObject
{
    public enum SatelliteType { scan, DPS }
    [Tooltip("Type of satellite")]
    public SatelliteType satelliteType;
    [Tooltip("Cost to buy")]
    public int cost;
    [Tooltip("Value associated with the satellite.")]
    public int value;

    public string satelliteName, description;
    
    public Sprite sprite;

    public void Add()
    {
        PlanetManager.AddSatellite(this);
    }
}
